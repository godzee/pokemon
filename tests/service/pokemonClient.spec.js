const pokemonClient = require('./../../service/pokemonClient')
const chai = require("chai");
const expect = chai.expect;


describe('Fetch Pokemans By Name', function () {
    it('Should return same lenght as pokemon names', () => {
        const pokemonNames = ['bulbasaur', 'ditto', 'machop', 'eevee'];
        return pokemonClient.getPokemonByNames(pokemonNames).then(function (data) {
            expect(data).to.have.lengthOf(4);
        });
    });
});