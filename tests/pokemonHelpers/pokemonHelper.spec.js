const pokemonHelper = require('./../../pokemonHelpers/pokemonHelper')
const chai = require("chai");
const expect = chai.expect;

describe('More Powerful Pokemon Test - Pokemon With More Moves', () => {
    it('it should return more powerful pokemon', () => {
        const mockResponse = {
            "name": "machop",
            "moves": 94
        };
        const req = [{ name: 'bulbasaur', moves: 78 },
        { name: 'ditto', moves: 1 },
        { name: 'machop', moves: 94 },
        { name: 'eevee', moves: 64 }];

        let res = pokemonHelper.getPokemonWithMostMoves(req);
        expect(res).to.eql(mockResponse);
    });
});

