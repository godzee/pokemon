module.exports = class AppError {
    /**
     * 
     * @param {String} code 
     * @param {String} errorMessage 
     */
    constructor(code, errorMessage) {
        this.code = code;
        this.errorMessage = errorMessage;
    }

}
