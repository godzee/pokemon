module.exports = class AppResponse {
    /**
     * @param {ResponseCode} code 
     * @param {Object} response 
     */
    constructor(code, response) {
        this.code = code;
        this.response = response;
    }
} 
