
# Pokemon
Pokemon - RESTful APIs


Pokemon is a Node.js application built using [Express](http://express.com/) framework.
The application purpose is to accepts a list of Pokemon names and returns the more powerful one.


## Setup
Download Docker Desktop for Mac or Windows. Docker Compose will be automatically installed. On Linux, make sure you have the latest version of Compose.
~~~~
$ docker-compose up
~~~~

## Debug on Visual Studio Code

It's easy to debug with Visual Studio Code both for unit tests and app run.
Add a file named "launch.json" in .vscode hidden folder, at the root of your project.
Paste the following content:

~~~ json

{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Debug Mocha tests",
            "type": "node",
            "request": "launch",
            "program": "${workspaceRoot}/node_modules/mocha/bin/_mocha",
            "stopOnEntry": false,
            "args": ["-t", "10000", "test/**/*.spec.js"],
            "cwd": "${workspaceRoot}",
            "preLaunchTask": null,
            "runtimeArgs": [
                "--nolazy"
            ],
            "env": {
                "NODE_ENV": "test"
            },
            "console": "internalConsole",
            "outFiles": [
                "${workspaceRoot}"
            ]
        },
        {
            "name": "Debug Program",
            "type": "node",
            "request": "launch",
            "program": "${workspaceRoot}/app.js"
        }
    ]
}

~~~

Now, if you want to run the application in debug mode (Ctr+Shift+D), choose one of the two available configurations ("Debug Mocha tests" or "Debug Program") and play "Start Debugging".
App will stop at your breakpoints.


