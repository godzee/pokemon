const Pokedex = require('pokedex-promise-v2');
const AppError = require('./../models/AppError');
const ResponseCode = require('./../models/ResponseCode');

const options = {
    cacheLimit: 3600 * 1000, // 1 hour
    timeout: 5 * 1000 // 5 seconds
}
const pokemon = new Pokedex(options);

async function getPokemonByNames(names) {

    try {
        let response = await pokemon.getPokemonByName(names);
        response = response.map(response => {
            return {
                "name": response.name,
                "moves": response.moves.length,
            }
        })
        return response;
    } catch (error) {
        return new AppError(ResponseCode.INVALID_REQUEST, "Kindly confirm the names");

    }
}

module.exports.getPokemonByNames = getPokemonByNames;