var express = require('express');
var router = express.Router();

const pokemonController = require('./../controllers/pokemonController')

router.post('/powerful-pokemon', pokemonController.morePowerfulPokemon);

module.exports = router;