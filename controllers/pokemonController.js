const pokemonClient = require('../service/pokemonClient');
const pokemonHelper = require('../pokemonHelpers/pokemonHelper');
const AppResponse = require('./../models/AppResponse');
const AppError = require('./../models/AppError');
const ResponseCode = require('./../models/ResponseCode');

function morePowerfulPokemon(req, res, next) {
    let names = req.body.pokemonNames;
    pokemonClient.getPokemonByNames(names)
        .then((response) => {
            if(response instanceof AppError){
                return res.json(400, response);
            }
            let morePowerfulPokemon = pokemonHelper.getPokemonWithMostMoves(response);
            return res.json(200, new AppResponse(ResponseCode.SUCCESS, morePowerfulPokemon));

        })
        .catch((err) => {
            return res.json(200, new AppError(ResponseCode.INVALID_REQUEST, err));
           })
}

module.exports.morePowerfulPokemon = morePowerfulPokemon
