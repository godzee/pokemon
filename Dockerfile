FROM node:alpine

# Create app directory
WORKDIR /usr/app

COPY ./ ./

RUN npm install

EXPOSE 3000

CMD [ "npm", "start" ]