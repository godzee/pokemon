const express = require("express");
const bodyParser = require('body-parser');

const app = express();
const routes = require('./routes/pokemonRoutes');
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


app.use('/', routes);


app.use(function (req, res, next) {
    console.log(`${req.url} Not found`);
    return res.status(404).send({ message: `Route ${req.url} Not found.` });
});

app.use(function (err, req, res, next) {
    return res.status(500).send({ error: err });
});


app.listen(port, function () {
    console.log(`Pokeman App listening at http://localhost:${port}`);
  });
  
module.exports = app;
